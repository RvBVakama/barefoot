// a textured phong vertex shader
// version to compile for
#version 410
// the position from the object
layout( location = 0 ) in vec4 Position;
// the normal from the object
layout( location = 1 ) in vec4 Normal;
// the texture coordinace from the object
layout( location = 2 ) in vec2 TexCoord;
// the tangent from the object
layout( location = 3 ) in vec4 Tangent;

// texture coordinace to send to the fragment shader
out vec2 vTexCoord;
// the normal to send to the fragment shader
out vec3 vNormal;
// the tangent to send to the fragment shader
out vec3 vTangent;
// the perpendicular tangent to send to the fragment shader
out vec3 vBiTangent;
// position to send to the fragment shader
out vec4 vPosition;

uniform mat4 ProjectionViewModel;

// we need the model matrix seperate
uniform mat4 ModelMatrix;

// we need this matrix to transform the normal
uniform mat3 NormalMatrix;

void main()
{
    // assign the texture coordinace
    vTexCoord = TexCoord;
    // assign the model's position
    vPosition = ModelMatrix * Position;
    // assign the normals
    vNormal = NormalMatrix * Normal.xyz;
    // assign the tangent
    vTangent = NormalMatrix * Tangent.xyz;
    // assign the biTangent
    vBiTangent = cross(vNormal, vTangent) * Tangent.w;
    // assign the vertex position
    gl_Position = ProjectionViewModel * Position;
}
