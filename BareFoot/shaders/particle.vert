// VERTEX SHADER
#version 410
// the position from the object
in vec4 Position;
// the color from the object
in vec4 Colour;
// to give to the fragment shader
out vec4 vColour;
// the projection view model passed in from the application
uniform mat4 ProjectionViewModel;
void main() 
{
	// link the color from the object with the color to 
	// give to the fragment shader
	vColour = Colour; 
	// vertex position
	gl_Position = ProjectionViewModel * Position;
}
