// a simple textured shader
#version 410
// the position from the model
layout( location = 0 ) in vec4 Position;
// the texture coordinace from the model
layout( location = 2 ) in vec2 TexCoord;
// the texture coordinace to send to the fragment shader
out vec2 vTexCoord;
// the projection view from the applicaiton
uniform mat4 ProjectionViewModel;

void main()
{
    // assign the texture coordiance
    vTexCoord = TexCoord;
    // assign the vertex position
    gl_Position = ProjectionViewModel * Position;
}
