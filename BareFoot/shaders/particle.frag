// FRAGMENT SHADER
// version to compile for
#version 410
// the color from the vertex shader
in vec4 vColour;
// the color to send to the application
out vec4 FragColour;
void main()
{
	// assign the color passes in to the
	// vertex shader to the application
	FragColour = vColour;
}
