// blends texture from differnt files into one based on 
// a rgb input texture
// version to compile for
#version 410
// the grass texture passed in from the application
uniform sampler2D grassTexture;
// the cobble texture passed in from the application
uniform sampler2D cobbleTexture;
// the river texture passed in from the application
uniform sampler2D riverTexture;
// the tint texture passed in from the application
uniform sampler2D tintTexture;
// the texture coordiance from the model
in vec2 vTexCoord;
// the color to send to the application
out vec4 pixelColour;

// time passed in from the application
uniform float time;
// the offset multiplier passed in from the application
uniform float offsetMultiplier = 0.3f;
// the offset speed passed in from the application
uniform float offsetSpeed = 0.3f;

void main( ) 
{
    // defining the textures as vec4s 
    vec4 grass;
    vec4 path;
    vec4 river;

    // assigning the blend texture
    vec4 blend = texture(tintTexture, vTexCoord);
    
    // create the grass around the path
    grass = texture( grassTexture, vTexCoord ) * (blend.g);

    // create the path
    path = texture( cobbleTexture, vTexCoord ) * (blend.b);
    
    // move the river texture around over time based on speed variables
	vec2 offsetTexCood;
	offsetTexCood.x = vTexCoord.x + sin(time * offsetSpeed) * offsetMultiplier;
	offsetTexCood.y = vTexCoord.y + cos(time * offsetSpeed) * offsetMultiplier;

    // create the river
    river = texture( riverTexture, offsetTexCood ) * (blend.r);

    // put them together
    pixelColour = grass + path + river;
}





















//// a phong map fragment shader
//#version 410
//uniform sampler2D grassTexture;
//uniform sampler2D pathTexture;
//
//in vec2 vTexCoord;
//in vec3 vNormal;
//in vec3 vTangent;
//in vec3 vBiTangent;
//in vec4 vPosition;
//out vec4 FragColour;
//uniform sampler2D diffuseTexture;
//uniform sampler2D specularTexture;
//uniform sampler2D normalTexture;
//
//uniform vec3 Ka; // material ambient
//uniform vec3 Kd; // material diffuse
//uniform vec3 Ks; // material specular
//uniform float specularPower;
//
//uniform vec3 Ia; // light ambient
//uniform vec3 Id; // light diffuse
//uniform vec3 Is; // light specular
//uniform vec3 LightDirection;
//uniform vec3 cameraPosition;
//
//void main()
//{
//    vec3 pixelColour = texture( grassTexture, vTexCoord ) * texture( pathTexture, vTexCoord );
//
//    vec3 N = normalize(vNormal);
//    vec3 T = normalize(vTangent);
//    vec3 B = normalize(vBiTangent);
//    vec3 L = normalize(LightDirection);
//    mat3 TBN = mat3(T,B,N);
//    
//    vec3 texDiffuse = pixelColour;
//    vec3 texSpecular = texture( specularTexture, vTexCoord ).rgb;
//    vec3 texNormal = texture( normalTexture, vTexCoord ).rgb;
//
//    N = TBN * (texNormal * 2 - 1);
//
//    // calculate lambert term
//    float lambertTerm = max( 0, dot( N, -L ) );
//
//    // calculate view vector and reflection vector
//    vec3 V = normalize(cameraPosition - vPosition.xyz);
//    vec3 R = normalize(reflect( L, N ));
//
//    // calculate specular term
//    float specularTerm = pow(max(0, dot( R, V )), specularPower);
//
//    // calculate each light property
//    vec3 ambient = Ia * Ka * texDiffuse;
//    vec3 diffuse = Id * Kd * texDiffuse * lambertTerm;
//    vec3 specular = Is * Ks * texSpecular * specularTerm;
//    FragColour = vec4(ambient + diffuse + specular, 1);
//}
//