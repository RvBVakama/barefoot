// a phong map fragment shader
// version to compile for
#version 410
// texture coordinance from the vertex shader
in vec2 vTexCoord;
// normal from the vertex shader
in vec3 vNormal;
// tangent from the vertex shader
in vec3 vTangent;
// biTangent from the vertex shader
in vec3 vBiTangent;
// position from the vertex shader
in vec4 vPosition;
// color to give to the application
out vec4 FragColour;
// base color texture
uniform sampler2D diffuseTexture;
// how shiny texture
uniform sampler2D specularTexture;
// detail texture
uniform sampler2D normalTexture;

uniform vec3 Ka; // material ambient
uniform vec3 Kd; // material diffuse
uniform vec3 Ks; // material specular
// how strong the shiny is
uniform float specularPower;

uniform vec3 Ia; // light ambient
uniform vec3 Id; // light diffuse
uniform vec3 Is; // light specular
// direction that the light faces
uniform vec3 LightDirection;
// the cameras position
uniform vec3 cameraPosition;

void main()
{
    // normalize the normal, tangent, biTangent and light direction
    // create the TBN from them
    vec3 N = normalize(vNormal);
    vec3 T = normalize(vTangent);
    vec3 B = normalize(vBiTangent);
    vec3 L = normalize(LightDirection);
    // transform textures into model space
    mat3 TBN = mat3(T,B,N);
    
    // setting the diffuse up
    vec3 texDiffuse = texture( diffuseTexture, vTexCoord ).rgb;
	// if the shader sees transparant surfaces discard that data
    float alpha = texture( diffuseTexture, vTexCoord ).a;
	if(alpha < 0.5)
	discard;

    // setting up the specular and normal maps
    vec3 texSpecular = texture( specularTexture, vTexCoord ).rgb;
    vec3 texNormal = texture( normalTexture, vTexCoord ).rgb;

    // calculating normalized normal
    N = TBN * (texNormal * 2 - 1);

    // calculate lambert term
    float lambertTerm = max( 0, dot( N, -L ) );

    // calculate view vector and reflection vector
    vec3 V = normalize(cameraPosition - vPosition.xyz);
    vec3 R = normalize(reflect( L, N ));

    // calculate specular term
    float specularTerm = pow(max(0, dot( R, V )), specularPower);

    // calculate each light property
    vec3 ambient = Ia * Ka * texDiffuse;
    vec3 diffuse = Id * Kd * texDiffuse * lambertTerm;
    vec3 specular = Is * Ks * texSpecular * specularTerm;
    FragColour = vec4(ambient + diffuse + specular, 1);
}
