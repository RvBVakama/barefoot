#pragma once

#include "Texture.h"

// using the aie namespace
namespace aie
{

	//--------------------------------------------------------------------------------------
	// RenderTarget object
	// Creates another buffer in which we can store images/textures. 
	// This allows us to render a frame while holding one in this buffer.
	// This allows us to apply effects to it or use it as a mirror's reflection.
	//--------------------------------------------------------------------------------------
	class RenderTarget
	{
	public:
		//--------------------------------------------------------------------------------------
		// Default Constructor
		//--------------------------------------------------------------------------------------
		RenderTarget();

		//--------------------------------------------------------------------------------------
		// Alternative Constructor
		// 
		// Parameters:
		//		targetCount: How many images/texture bufers to create.
		//		width: width of those images/textures buffers.
		//		height: height of those images/textures buffers.
		//--------------------------------------------------------------------------------------
		RenderTarget(unsigned int targetCount, unsigned int width, unsigned int height);

		//--------------------------------------------------------------------------------------
		// Default Destructor
		//--------------------------------------------------------------------------------------
		virtual ~RenderTarget();

		//--------------------------------------------------------------------------------------
		// Runs initialize() and checks if the window should close if esc was pressed.
		//
		// Parameters:
		//		targetCount: How many images/texture bufers to create.
		//		width: width of those images/textures buffers.
		//		height: height of those images/textures buffers.
		//
		// Returns true or false if it ran successfully or not.
		//--------------------------------------------------------------------------------------
		bool initialise(unsigned int targetCount, unsigned int width, unsigned int height);

		//--------------------------------------------------------------------------------------
		// Binds the framebuffer to the frame buffer object that we created.
		//--------------------------------------------------------------------------------------
		void bind();

		//--------------------------------------------------------------------------------------
		// Unbinds the framebuffer by setting it to zero.
		//--------------------------------------------------------------------------------------
		void unbind();

		//--------------------------------------------------------------------------------------
		// Gets the width of the frame buffer object.
		//
		//	Returns the width of the buffer object.
		//--------------------------------------------------------------------------------------
		unsigned int	getWidth() const { return m_width; }

		//--------------------------------------------------------------------------------------
		// Gets the height of the frame buffer object.
		//
		//	Returns the height of the buffer object.
		//--------------------------------------------------------------------------------------
		unsigned int	getHeight() const { return m_height; }

		//--------------------------------------------------------------------------------------
		// Gets the handle of the frame buffer object.
		//
		//	Returns the width of the buffer object.
		//--------------------------------------------------------------------------------------

		unsigned int	getFrameBufferHandle() const { return m_fbo; }

		//--------------------------------------------------------------------------------------
		// Gets the target count of frame buffer objects.
		//
		//	Returns the target count of frame buffer objects.
		//--------------------------------------------------------------------------------------
		unsigned int	getTargetCount() const { return m_targetCount; }


		//--------------------------------------------------------------------------------------
		// Gets the target count of frame buffer objects.
		//
		//	Parameters:
		//		target: the texture to be drawn to the quad.
		//
		//	Returns the texture to be drawn to the quad.
		//--------------------------------------------------------------------------------------
		const Texture&	getTarget(unsigned int target) const { return m_targets[target]; }

		// variables needed to create a render target system.
	protected:
		unsigned int	m_width;
		unsigned int	m_height;

		unsigned int	m_fbo;
		unsigned int	m_rbo;

		unsigned int	m_targetCount;
		Texture*		m_targets;
	};
}