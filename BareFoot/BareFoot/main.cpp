#include <iostream>
#include <glm.hpp>
#include <ext.hpp>
#include "gl_core_4_5.h"
#include <glfw3.h>
#include "FootCore.h"
#include "Application.h"

// defining some usings
using glm::vec3;
using glm::vec4;
using glm::mat4;

//--------------------------------------------------------------------------------------
// starts the app with the defined parameters
//
// Return:
//		Returns 0 for success.
//--------------------------------------------------------------------------------------
int main()
{
	// create a new instance of the app
	auto app = new Application(glm::vec2(1280, 720), "Foot Viewer");

	// start the app
	app->start();

	// delete the app, clean memory
	delete app;

	return 0;
}
