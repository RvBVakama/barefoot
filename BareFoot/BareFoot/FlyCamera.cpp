#include "FlyCamera.h"

// using glm
using glm::vec3;
using glm::vec4;
using glm::mat4;

//--------------------------------------------------------------------------------------
// Default Constructor
// Sets default values.
//--------------------------------------------------------------------------------------
FlyCamera::FlyCamera()
{
	// assigning default values

	m_dMouseX = 0.0f;
	m_dMouseY = 0.0f;

	deltaMouseX = 0.0f;
	deltaMouseY = 0.0f;

	mouseX = 0.0f;
	mouseY = 0.0f;

	fMouseSensitivity = 2.5f;

	fMoveSpeed = 5.0f;
}

//--------------------------------------------------------------------------------------
// Default Destructor
//--------------------------------------------------------------------------------------
FlyCamera::~FlyCamera()
{
}

//--------------------------------------------------------------------------------------
// Runs all the nessasary stuff to makes the camera move.
//
//	Parameters:
//		deltaTime: delta time.
//		a_glfwWindow: access to the window.
//--------------------------------------------------------------------------------------
void FlyCamera::update(double deltaTime, GLFWwindow* a_GLWindow)
{
	// calculate the mouse's delta x and y
	glfwGetCursorPos(a_GLWindow, &m_dMouseX, &m_dMouseY);

	// calculate relative world up
	vec4 up = glm::inverse(worldTransform) * vec4(0, 1, 0, 0);
	mat4 rotMat(1);

	// if deltaMouseX is 0, when glm rotate is used, rotMat results in NaN which breaks the app
	if (-deltaMouseX != 0.0f)
	{
		// rotate around the world's up
		rotMat = glm::rotate((float(-deltaMouseX) * fMouseSensitivity)* float(deltaTime), vec3(up[0], up[1], up[2]));
		viewTransform = rotMat * viewTransform;
	}

	// rotate up and down
	rotMat = glm::rotate((float(-deltaMouseY) * fMouseSensitivity)* float(deltaTime), vec3(1, 0, 0));
	viewTransform = rotMat * viewTransform;

	// radians to degrees
	deltaMouseX = ((1280 / 2 - m_dMouseX) * 0.0174533);
	deltaMouseY = ((720 / 2 - m_dMouseY) * 0.0174533);

	// keep mouse locked to screen
	glfwSetCursorPos(a_GLWindow, 1280 / 2, 720 / 2);

	// hide the mouse
	glfwSetInputMode(a_GLWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	//std::cout << -deltaMouseX << std::endl;
	//std::cout << -deltaMouseY << std::endl;

	//update world transform
	worldTransform = glm::inverse(viewTransform);

	Controls(deltaTime, a_GLWindow);

	// making sure that the forward matrix is maintained
	worldTransform[3][3] = 1.0f;

	// update the view transform then the projection view
	viewTransform = glm::inverse(worldTransform);
	updateProjectionViewTransform();
}

//--------------------------------------------------------------------------------------
// Input controls to move the camera.
//
//	Parameters:
//		deltaTime: delta time.
//		a_glfwWindow: access to the window.
//--------------------------------------------------------------------------------------
void FlyCamera::Controls(double deltaTime, GLFWwindow* a_GLWindow)
{
	// keyboard input
	// move fowards, on the camera's z axis
	if (glfwGetKey(a_GLWindow, GLFW_KEY_W) == GLFW_PRESS)
	{
		worldTransform[3] += worldTransform[2] * deltaTime * -fMoveSpeed;
	}

	// move backwards, on the camera's z axis
	if (glfwGetKey(a_GLWindow, GLFW_KEY_S) == GLFW_PRESS)
	{
		worldTransform[3] += worldTransform[2] * deltaTime * fMoveSpeed;
	}

	// strafe left
	if (glfwGetKey(a_GLWindow, GLFW_KEY_A) == GLFW_PRESS)
	{
		worldTransform[3] += worldTransform[0] * deltaTime * -fMoveSpeed;
	}

	// strafe right
	if (glfwGetKey(a_GLWindow, GLFW_KEY_D) == GLFW_PRESS)
	{
		worldTransform[3] += worldTransform[0] * deltaTime * fMoveSpeed;
	}

	// raise upwards, on the camera's y axis
	if (glfwGetKey(a_GLWindow, GLFW_KEY_E) == GLFW_PRESS)
	{
		worldTransform[3] += worldTransform[1] * deltaTime * fMoveSpeed;
	}

	// fall downwards, on the camera's y axis
	if (glfwGetKey(a_GLWindow, GLFW_KEY_Q) == GLFW_PRESS)
	{
		worldTransform[3] += worldTransform[1] * deltaTime * -fMoveSpeed;
	}

	// fly faster with shift
	if (glfwGetKey(a_GLWindow, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		fMoveSpeed = 20.0f;
		fMouseSensitivity = 5.0f;
	}
	else
	{
		fMoveSpeed = 5.0f;
		fMouseSensitivity = 2.5f;
	}
	// debug set pos to centre of world 
	if (glfwGetKey(a_GLWindow, GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		setPos(vec3(0, 0, 0));
	}

	// changes the fov with each button from 1 to 9
	if (glfwGetKey(a_GLWindow, GLFW_KEY_1) == GLFW_PRESS)
		setPerspective(glm::pi<float>() * fovAmount.one, 16.0f / 9.0f, 0.1f, 1000.0f);
	if (glfwGetKey(a_GLWindow, GLFW_KEY_2) == GLFW_PRESS)
		setPerspective(glm::pi<float>() * fovAmount.two, 16.0f / 9.0f, 0.1f, 1000.0f);
	if (glfwGetKey(a_GLWindow, GLFW_KEY_3) == GLFW_PRESS)
		setPerspective(glm::pi<float>() * fovAmount.three, 16.0f / 9.0f, 0.1f, 1000.0f);
	if (glfwGetKey(a_GLWindow, GLFW_KEY_4) == GLFW_PRESS)
		setPerspective(glm::pi<float>() * fovAmount.four, 16.0f / 9.0f, 0.1f, 1000.0f);
	if (glfwGetKey(a_GLWindow, GLFW_KEY_5) == GLFW_PRESS)
		setPerspective(glm::pi<float>() * fovAmount.five, 16.0f / 9.0f, 0.1f, 1000.0f);
	if (glfwGetKey(a_GLWindow, GLFW_KEY_6) == GLFW_PRESS)
		setPerspective(glm::pi<float>() * fovAmount.six, 16.0f / 9.0f, 0.1f, 1000.0f);
	if (glfwGetKey(a_GLWindow, GLFW_KEY_7) == GLFW_PRESS)
		setPerspective(glm::pi<float>() * fovAmount.seven, 16.0f / 9.0f, 0.1f, 1000.0f);
	if (glfwGetKey(a_GLWindow, GLFW_KEY_8) == GLFW_PRESS)
		setPerspective(glm::pi<float>() * fovAmount.eight, 16.0f / 9.0f, 0.1f, 1000.0f);
	if (glfwGetKey(a_GLWindow, GLFW_KEY_9) == GLFW_PRESS)
		setPerspective(glm::pi<float>() * fovAmount.nine, 16.0f / 9.0f, 0.1f, 1000.0f);
	if (glfwGetKey(a_GLWindow, GLFW_KEY_0) == GLFW_PRESS)
		setPerspective(glm::pi<float>() * fovAmount.ten, 16.0f / 9.0f, 0.1f, 1000.0f);
}

