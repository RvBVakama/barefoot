#pragma once
#include <glm.hpp>
#include <glfw3.h>

using glm::vec3;
using glm::vec4;
using glm::mat4;

//--------------------------------------------------------------------------------------
// Fov object
// Handles the fov range, press 1 through to 9!
//--------------------------------------------------------------------------------------
struct Fov
{
public:
	float original = 0.25f;

	float one = 0.05f;
	float two = 0.15f;
	float three = 0.25f;
	float four = 0.35f;
	float five = 0.45f;
	float six = 0.55f;
	float seven = 0.65f;
	float eight = 0.75f;
	float nine = 0.85f;
	float ten = 0.95f;
};

//--------------------------------------------------------------------------------------
// Camera object
// base camera script that defines base camera vars
//--------------------------------------------------------------------------------------
class Camera
{
public:

	//--------------------------------------------------------------------------------------
	// Default Constructor
	// Sets up default values.
	//--------------------------------------------------------------------------------------
	Camera();

	//--------------------------------------------------------------------------------------
	// Default Destructor
	//--------------------------------------------------------------------------------------
	~Camera();

	//--------------------------------------------------------------------------------------
	// Pure virtual function.
	//
	//	Parameters:
	//		deltaTime: delta time.
	//		a_glfwWindow: access to the window.
	//--------------------------------------------------------------------------------------
	virtual void update(double deltaTime, GLFWwindow* a_glfwWindow) = 0;

	//--------------------------------------------------------------------------------------
	// Sets the perspective of the camera.
	//
	//	Parameters:
	//		fieldOfView: FOV.
	//		aspectRatio: aspect ratio.
	//		near: near clip plane.
	//		far: far clip plane.
	//--------------------------------------------------------------------------------------
	void setPerspective(const float &fieldOfView, const float &aspectRatio, const float &near, const float &far);

	//--------------------------------------------------------------------------------------
	// Sets the perspective of the camera.
	//
	//	Parameters:
	//		from: position where it looks from.
	//		to: where it will look.
	//		up: define its y axis.
	//--------------------------------------------------------------------------------------
	void setLookAt(const vec3 &from, const vec3 &to, const vec3 &up);

	//--------------------------------------------------------------------------------------
	// Sets the position of the camera.
	//
	//	Parameter:
	//		position: position where it goes to.
	//--------------------------------------------------------------------------------------
	void setPos(const vec3 &position);

	//--------------------------------------------------------------------------------------
	// Gets the world transform.
	//
	// Return:
	//		Returns the world transform.
	//--------------------------------------------------------------------------------------
	mat4 getWorldTransform() const;

	//--------------------------------------------------------------------------------------
	// Gets the view transform.
	//
	// Return:
	//		Returns the view transform.
	//--------------------------------------------------------------------------------------
	mat4 getView() const;

	//--------------------------------------------------------------------------------------
	// Gets the projection transform.
	//
	// Return:
	//		Returns the projection transform.
	//--------------------------------------------------------------------------------------
	mat4 getProjection() const;

	//--------------------------------------------------------------------------------------
	// Gets the projection View Transform.
	//
	// Return:
	//		Returns the projection View Transform.
	//--------------------------------------------------------------------------------------
	mat4 getProjectionView() const;

	//--------------------------------------------------------------------------------------
	// Gets the camera's position.
	//
	// Return:
	//		Returns the camera's position
	//--------------------------------------------------------------------------------------
	vec3 getPosition();

protected:

	// the transforms that help us see
	mat4 worldTransform;
	mat4 viewTransform;
	mat4 projectionTransform;
	mat4 projectionViewTransform;

	//--------------------------------------------------------------------------------------
	// updates the projection view transform
	//--------------------------------------------------------------------------------------
	void updateProjectionViewTransform();
	float fov = 0;

	Fov fovAmount;
};

