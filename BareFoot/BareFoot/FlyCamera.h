#pragma once
#include "Application.h"
#include "Camera.h"

//--------------------------------------------------------------------------------------
// FlyCamera object
// fly camera script that allows the user to control it
//--------------------------------------------------------------------------------------
class FlyCamera : public Camera
{
public:

	//--------------------------------------------------------------------------------------
	// Default Constructor
	//--------------------------------------------------------------------------------------
	FlyCamera();

	//--------------------------------------------------------------------------------------
	// Default Destructor
	//--------------------------------------------------------------------------------------
	~FlyCamera();

	//--------------------------------------------------------------------------------------
	// Runs all the nessasary stuff to makes the camera move.
	//
	//	Parameters:
	//		deltaTime: delta time.
	//		a_glfwWindow: access to the window.
	//--------------------------------------------------------------------------------------
	void update(double deltaTime, GLFWwindow * a_GLWindow);

	//--------------------------------------------------------------------------------------
	// Input controls to move the camera.
	//
	//	Parameters:
	//		deltaTime: delta time.
	//		a_glfwWindow: access to the window.
	//--------------------------------------------------------------------------------------
	void Controls(double deltaTime, GLFWwindow* a_GLWindow);

	// variables and properties that make up the fly camera
	double m_dMouseX;
	double m_dMouseY;

	double deltaMouseX;
	double deltaMouseY;

	float mouseX;
	float mouseY;

	float fMouseSensitivity;

	float fMoveSpeed;

	Fov fovAmount;
};

