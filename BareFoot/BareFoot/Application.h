
#pragma once
#include "FootCore.h"
#include "FlyCamera.h"
#include "Shader.h"
#include "Mesh.h"
#include "OBJMesh.h"
#include "RenderTarget.h"
#include "ParticleEmitter.h"

// foward declare camera
class FlyCamera;


//--------------------------------------------------------------------------------------
// Application object
// Ties everything from other classes together, and defines meshes and their 
// draw properties.
//--------------------------------------------------------------------------------------
class Application
{
public:

	//--------------------------------------------------------------------------------------
	// Alternative Constructor
	// 
	// Parameters:
	//		a_resolution: screen resolution.
	//		a_name: name of window.
	//--------------------------------------------------------------------------------------
	Application(const glm::ivec2& a_resolution, const char* a_name);

	//--------------------------------------------------------------------------------------
	// Default Destructor
	//--------------------------------------------------------------------------------------
	~Application();

	//--------------------------------------------------------------------------------------
	// Clears the screen to the glColor.
	//--------------------------------------------------------------------------------------
	void clearScreen();

	//--------------------------------------------------------------------------------------
	// Runs initialize() and checks if the window should close if esc was pressed.
	//
	// Return:
	//		Returns 0 for success.
	//--------------------------------------------------------------------------------------
	int start();

	// for making deltaTime
	foot::time m_currentTime;
	foot::time m_previousTime;

protected:

	//--------------------------------------------------------------------------------------
	// Starts the application and creates all nessarasy data to be used later on such as:
	// the glfw window, the screen count, makes the spawned window current, loads ogl funcs
	// and runs all the initialise functions for meshes to be drawn.
	//
	// Return:
	//		Returns 0 for success or -1 if it failed.
	//--------------------------------------------------------------------------------------
	int initialize();

	//--------------------------------------------------------------------------------------
	// Update, calculates deltaTime, draws the grid, calculates light, and runs the 
	// draw function
	//
	// Parameters:
	//		deltaTime: the time it took for the last frame render.
	// 
	// Return:
	//		Returns true for success.
	//--------------------------------------------------------------------------------------
	bool update(double deltaTime);

	//--------------------------------------------------------------------------------------
	// Draws all initialised meshes.
	//--------------------------------------------------------------------------------------
	void draw();

	//--------------------------------------------------------------------------------------
	// cleans up the application, destroys the window, terminates glfw
	//
	// Return:
	//		Returns 0 for success.
	//--------------------------------------------------------------------------------------
	int terminate();

	//--------------------------------------------------------------------------------------
	// Links the appropriate shader and mesh then sets it location and scale.
	//
	// Return:
	//		Returns false if the shader is invalid or the model cannot be loaded.
	//--------------------------------------------------------------------------------------
	bool InitTexPhongSpear();

	//--------------------------------------------------------------------------------------
	// Draws the model applied in the function above.
	//--------------------------------------------------------------------------------------
	void DrawTexPhongSpear();

	//--------------------------------------------------------------------------------------
	// Links the appropriate shader and mesh then sets it location and scale.
	//
	// Return:
	//		Returns false if the shader is invalid or the model cannot be loaded.
	//--------------------------------------------------------------------------------------
	bool InitTexPhongRock();

	//--------------------------------------------------------------------------------------
	// Draws the model applied in the function above.
	//--------------------------------------------------------------------------------------
	void DrawTexPhongRock();

	//--------------------------------------------------------------------------------------
	// Links the appropriate shader and mesh then sets it location and scale.
	//
	// Return:
	//		Returns false if the shader is invalid or the model cannot be loaded.
	//--------------------------------------------------------------------------------------
	bool InitTexPhongArthur();

	//--------------------------------------------------------------------------------------
	// Draws the model applied in the function above.
	//--------------------------------------------------------------------------------------
	void DrawTexPhongArthur();

	//--------------------------------------------------------------------------------------
	// Links the appropriate shader and mesh then sets it location and scale.
	//
	// Return:
	//		Returns false if the shader is invalid or the model cannot be loaded.
	//--------------------------------------------------------------------------------------
	bool InitTexBlendQuad();

	//--------------------------------------------------------------------------------------
	// Draws the model applied in the function above.
	//--------------------------------------------------------------------------------------
	void DrawTexBlendQuad();

	//--------------------------------------------------------------------------------------
	// Links the appropriate shader and mesh then sets it location and scale.
	//
	// Return:
	//		Returns false if the shader is invalid or the model cannot be loaded.
	//--------------------------------------------------------------------------------------
	bool InitTexTree();

	//--------------------------------------------------------------------------------------
	// Draws the model applied in the function above.
	//--------------------------------------------------------------------------------------
	void DrawTexTree();

	//--------------------------------------------------------------------------------------
	// Links the appropriate shader and mesh then sets it location and scale.
	//
	// Return:
	//		Returns false if the shader is invalid or the model cannot be loaded.
	//--------------------------------------------------------------------------------------
	bool InitTexTree2();

	//--------------------------------------------------------------------------------------
	// Draws the model applied in the function above.
	//--------------------------------------------------------------------------------------
	void DrawTexTree2();

	//--------------------------------------------------------------------------------------
	// Links the appropriate shader and creates a particle system emmiter then sets it spawn
	// location and scale.
	//
	// Return:
	//		Returns false if the shader is invalid.
	//--------------------------------------------------------------------------------------
	bool InitParticles();

	//--------------------------------------------------------------------------------------
	// Draws the particles.
	//--------------------------------------------------------------------------------------
	void DrawParticles();

	//--------------------------------------------------------------------------------------
	// Initialises the render target and and prepares it to be drawn.
	//
	// Return:
	//		Returns false if the shader cannot be loaded.
	//--------------------------------------------------------------------------------------
	bool InitRenderTargetTex();

	//--------------------------------------------------------------------------------------
	// Draws the buffer the quad we are using.
	//--------------------------------------------------------------------------------------
	void DrawRenderTargetTex();

	double m_deltaTime;
	
	// glfw
	glm::ivec2 m_windowResolution;
	const char* m_windowName;
	GLFWmonitor** screens;
	int screenCount = 0;
	GLFWwindow* window;

	// time
	foot::clock m_clock;
	foot::time m_startTime;

	// the camera player controls
	FlyCamera* flycam;

	aie::Texture m_gridTexture;

	// shaders
	aie::ShaderProgram	m_shader;
	aie::ShaderProgram	m_texturedShader;
	aie::ShaderProgram	m_phongShader;
	aie::ShaderProgram	m_blendShader;
	aie::ShaderProgram	m_particleShader;

	// textured quad
	Mesh				m_texQuadMesh;
	glm::mat4			m_texQuadTransform;

	// phong quad
	Mesh				m_phongQuadMesh;
	glm::mat4			m_phongQuadTransform;

	// bunny
	aie::OBJMesh		m_bunnyMesh;
	glm::mat4			m_bunnyTransform;

	// lucy
	aie::OBJMesh		m_lucyMesh;
	glm::mat4			m_lucyTransform;

	// spear
	aie::ShaderProgram	m_normalMapShader;
	aie::OBJMesh		m_spearMesh;
	glm::mat4			m_spearTransform;

	// rock
	aie::OBJMesh		m_rockMesh;
	glm::mat4			m_rockTransform;

	// arthur
	aie::OBJMesh		m_arthurMesh;
	glm::mat4			m_arthurTransform;

	// blend quad
	Mesh				m_blendQuadMesh;
	glm::mat4			m_blendQuadTransform;

	// tree
	aie::OBJMesh		m_treeMesh;
	glm::mat4			m_treeTransform;

	// tree 2
	aie::OBJMesh		m_tree2Mesh;
	glm::mat4			m_tree2Transform;

	// blend quad
	aie::Texture grass;
	aie::Texture path;
	aie::Texture river;
	aie::Texture blendMap;

	// particles
	glm::mat4			m_particleTransform;

	// render target quad
	Mesh				m_renderTarQuadMesh;
	glm::mat4			m_renderTarQuadTransform;


	//--------------------------------------------------------------------------------------
	// Light object
	// Handles phong lighting
	//--------------------------------------------------------------------------------------
	struct Light
	{
		glm::vec3 direction;
		glm::vec3 diffuse;
		glm::vec3 specular;

	};

	// light 1
	Light				m_light1;
	glm::vec3			m_ambientLight1;
	// light 2
	Light				m_light2;
	glm::vec3			m_ambientLight2;
	// light 3
	Light				m_light3;
	glm::vec3			m_ambientLight3;
	// light 4
	Light				m_light4;
	glm::vec3			m_ambientLight4;
	// light 5
	Light				m_light5;
	glm::vec3			m_ambientLight5;

	// render targets
	aie::RenderTarget	m_renderTarget;

	// texture to render to
	aie::Texture renderTargetTexture;

	// particle system
	ParticleEmitter* m_emitter;
};

