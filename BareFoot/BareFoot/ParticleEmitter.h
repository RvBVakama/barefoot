#pragma once
#include "FootCore.h"

// defining some usings
using glm::vec3;
using glm::vec4;
using glm::mat4;

//--------------------------------------------------------------------------------------
// Particle object
// Defines a particle.
//--------------------------------------------------------------------------------------
struct Particle
{
	vec3 position;
	vec3 velocity;
	vec4 colour;
	float size;
	float lifetime;
	float lifespan;
};

//--------------------------------------------------------------------------------------
// ParticleVertex object
// Defines position and colour.
//--------------------------------------------------------------------------------------
struct ParticleVertex
{
	vec4 position;
	vec4 colour;
};

//--------------------------------------------------------------------------------------
// ParticleEmitter object
// Defines handles emitting particles
//--------------------------------------------------------------------------------------
class ParticleEmitter
{
public:
	//--------------------------------------------------------------------------------------
	// Default Constructor
	//--------------------------------------------------------------------------------------
	ParticleEmitter();

	//--------------------------------------------------------------------------------------
	// Default Destructor
	//--------------------------------------------------------------------------------------
	virtual ~ParticleEmitter();

	//--------------------------------------------------------------------------------------
	// Initialise takes in the desired particle settings and sets the up for usage.
	// 
	// Parameters:
	//		a_maxParticles: how many particles can exist at once.
	//		a_emitRate: how fast particles are emitted.
	//		a_lifetimeMin: how short a particle can last before its destroyed.
	//		a_lifetimeMax: how long a particle can last before its destroyed.
	//		a_velocityMin: how slow any one particle can travel.
	//		a_velocityMax: how fast any one particle can travel.
	//		a_startSize: how big is the particle at the start.
	//		a_endSize: how big should the particle be at the end.
	//		a_startColour: what color should the particle be at the start of its life.
	//		a_endColour: what color should the particle be at the end of its life.
	//--------------------------------------------------------------------------------------
	void initalise(unsigned int a_maxParticles, unsigned int a_emitRate, float a_lifetimeMin,
		float a_lifetimeMax, float a_velocityMin, float a_velocityMax, float a_startSize,
		float a_endSize, const vec4 & a_startColour, const vec4 & a_endColour);

	//--------------------------------------------------------------------------------------
	// Emits the particles, assigns the start position, randomizes the lifespan, sets color.
	//--------------------------------------------------------------------------------------
	void emit();

	//--------------------------------------------------------------------------------------
	// Update, updates the particles so they can propagate throughout the scene.
	// 
	// Parameters:
	//		a_deltaTime: how long it took to render the last frame.
	//		a_cameraTransform: the cameras transform, so we can render the particles.
	//--------------------------------------------------------------------------------------
	void update(float a_deltaTime, const mat4 & a_cameraTransform);

	//--------------------------------------------------------------------------------------
	// Draws the particles, syncing the buffer based on how many particles are active.
	//--------------------------------------------------------------------------------------
	void draw();

	// defining all the properties that can be manipulated to create many types of particle 
	// systems
protected:
	Particle * m_particles;
	unsigned int m_firstDead;
	unsigned int m_maxParticles;
	unsigned int m_vao, m_vbo, m_ibo;
	ParticleVertex* m_vertexData;
	vec3 m_position;
	float m_emitTimer;
	float m_emitRate;
	float m_lifespanMin;
	float m_lifespanMax;
	float m_velocityMin;
	float m_velocityMax;
	float m_startSize;
	float m_endSize;
	vec4 m_startColour;
	vec4 m_endColour;

};