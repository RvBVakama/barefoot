#pragma once

#include <glm.hpp>

//--------------------------------------------------------------------------------------
// Mesh Object 
// Handles creating quads that can drawn to, placed and scaled.
//--------------------------------------------------------------------------------------
class Mesh
{
public:

	//--------------------------------------------------------------------------------------
	// Default Constructor
	// Sets initial values.
	//--------------------------------------------------------------------------------------
	Mesh() : triCount(0), vao(0), vbo(0), ibo(0) {}
	
	//--------------------------------------------------------------------------------------
	// Default Destructor
	//--------------------------------------------------------------------------------------
	virtual ~Mesh();

	//--------------------------------------------------------------------------------------
	// Vertex Object 
	// Verticies can have a position a normal and a texture coordinance.
	//--------------------------------------------------------------------------------------
	struct Vertex
	{
		glm::vec4 position;
		glm::vec4 normal;
		glm::vec2 texCoord;
	};

	//--------------------------------------------------------------------------------------
	// Creates a placeable, scalable, render-to-able predefined sized quad.
	//--------------------------------------------------------------------------------------
	void initialiseQuad();

	//--------------------------------------------------------------------------------------
	// Draws quads to the screen.
	//--------------------------------------------------------------------------------------
	virtual void draw();

protected:

	// how many triangles in the quad
	unsigned int triCount;
	// vertex array object
	unsigned int vao;
	// vertex buffer object
	unsigned int vbo;
	// indicies buffer object
	unsigned int ibo;
	// defining a pointer to verticies
	Vertex* vertices = nullptr;
};

