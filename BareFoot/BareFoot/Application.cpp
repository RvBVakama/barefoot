#include "Application.h"

// defining some usings from glm
using glm::vec3;
using glm::vec4;
using glm::mat4;

//--------------------------------------------------------------------------------------
// Alternative Constructor
// 
// Parameters:
//		a_resolution: screen resolution.
//		a_name: name of window.
//--------------------------------------------------------------------------------------
Application::Application(const glm::ivec2& a_resolution, const char* a_name)
{
	m_windowResolution = a_resolution;
	m_windowName = a_name;
	flycam = new FlyCamera();
}

//--------------------------------------------------------------------------------------
// Default Destructor
//--------------------------------------------------------------------------------------
Application::~Application()
{

}

//--------------------------------------------------------------------------------------
// Clears the screen to the glColor.
//--------------------------------------------------------------------------------------
void Application::clearScreen()
{
	// clearing buffer, colour and depth checks
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

//--------------------------------------------------------------------------------------
// Runs initialize() and checks if the window should close if esc was pressed.
//
// Return:
//		Returns 0 for success.
//--------------------------------------------------------------------------------------
int Application::start()
{
	initialize();

	// if the window is not supposed to close and if the escape key was not pressed, then run the loop
	while (glfwWindowShouldClose(window) == false && glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS)
		update(m_deltaTime);

	return 0;
}

//--------------------------------------------------------------------------------------
// Starts the application and creates all nessarasy data to be used later on such as:
// the glfw window, the screen count, makes the spawned window current, loads ogl funcs
// runs all the initialise functions for meshes to be drawn.
//
// Return:
//		Returns 0 for success or -1 if it failed.
//--------------------------------------------------------------------------------------
int Application::initialize()
{
	// keeping track of time
	m_startTime = m_clock.now();
	m_currentTime = m_clock.now();
	m_previousTime = m_clock.now();

	// if glfw can connect to the gpu
	if (glfwInit() == false)
	{
		return -1;
	}

	// create an instance of a window with dimentions and a title but no moniter,
	// this means it chooses the primary display
	window = glfwCreateWindow(1280, 720, "barefoot graphics!", nullptr, nullptr);

	// save the amount of screens found
	screens = glfwGetMonitors(&screenCount);

	// if the window did not initialize
	if (!window)
	{
		// shutdown glfw
		glfwTerminate();
		return -2;
	}

	// make this window current
	glfwMakeContextCurrent(window);

	// remapping to the correct version and feature sets
	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		glfwDestroyWindow(window);
		glfwTerminate();
		return -3;
	}

	// testing for what version of open gl we are running
	auto major = ogl_GetMajorVersion();
	auto minor = ogl_GetMajorVersion();

	// clear the screen and sets the bg colour
	glClearColor(0.25f, 0.25f, 0.25f, 1);

	// enables the depth buffer
	glEnable(GL_DEPTH_TEST);

	//// RENDER TARGET EXAMPLE
	//if (m_renderTarget.initialise(1, 1208, 720) == false)
	//{
	//	printf("Render Target Error!\n");
	//		return false;
	//}
	//
	//InitRenderTargetTex();
	//// END EXAMPLE

	InitTexPhongSpear();
	
	InitTexPhongRock();

	InitTexPhongArthur();

	InitTexBlendQuad();

	InitTexTree();

	InitTexTree2();

	InitParticles();

	// light 1
	m_light1.diffuse = { 1, 1, 1 };
	m_light1.specular = { 1, 1, 1 };
	m_ambientLight1 = { 0.25f, 0.25f, 0.25f };

	// light 2
	m_light2.diffuse = { 1, 1, 1 };
	m_light2.specular = { 1, 1, 1 };
	m_ambientLight2 = { 0.25f, 0.25f, 0.25f };

	// light 3
	m_light3.diffuse = { 1, 1, 1 };
	m_light3.specular = { 1, 1, 1 };
	m_ambientLight3 = { 0.25f, 0.25f, 0.25f };

	// light 4
	m_light4.diffuse = { 1, 1, 1 };
	m_light4.specular = { 1, 1, 1 };
	m_ambientLight4 = { 0.25f, 0.25f, 0.25f };

	// light 5
	m_light5.diffuse = { 1, 1, 1 };
	m_light5.specular = { 1, 1, 1 };
	m_ambientLight5 = { 0.25f, 0.25f, 0.25f };

	return 0;
}

//--------------------------------------------------------------------------------------
// Update, calculates deltaTime, draws the grid, calculates light, and runs the 
// draw function
//
// Parameters:
//		deltaTime: the time it took for the last frame render.
// 
// Return:
//		Returns true for success.
//--------------------------------------------------------------------------------------
bool Application::update(double deltaTime)
{
	// calculating deltaTime
	m_previousTime = m_currentTime;
	m_currentTime = m_clock.now();
	auto duration = m_currentTime - m_previousTime;
	deltaTime = duration.count() * NANO_TO_SECONDS;

	// printing deltaTime info
	//printf("%d %f\n", duration.count(), deltaTime);

	// clean the screen ready to draw again
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// query time since the application started
	float time = (float)glfwGetTime();

	// rotate light 1
	m_light1.direction = glm::normalize(-vec3(glm::sin(time * 1), 0, glm::cos(time * 1)));
	// rotate light 2
	m_light2.direction = glm::normalize(vec3(glm::sin(time * 2), 0, glm::cos(time * 2)));
	// rotate light 3
	m_light3.direction = glm::normalize(-vec3(glm::sin(time * 3), 0, glm::cos(time * 3)));
	// rotate light 4
	m_light4.direction = glm::normalize(vec3(glm::sin(time * 2), 0, glm::cos(time * 2)));
	// rotate light 5
	m_light5.direction = glm::normalize(-vec3(glm::sin(time * 1), 0, glm::cos(time * 1)));

	// update the pretty particles
	m_emitter->update((float)deltaTime, flycam->getWorldTransform());

	// draw stuff
	draw();
	
	// run fly cam update
	flycam->update(deltaTime, window);

	// updates monitor by swapping the rendered back buffer
	glfwSwapBuffers(window);

	// so we can feed system input to glfw, keyboard keys pressed etc...
	glfwPollEvents();

	return true;
}

//--------------------------------------------------------------------------------------
// Draws all initialised meshes.
//--------------------------------------------------------------------------------------
void Application::draw()
{
	//// RENDER TARGET BLOCK
	//// anything you want to be rendered as a render target needs to 
	//// be between m_renderTarget.bind and m_renderTarget.unbind 
	////(which is in DrawRenderTargetTex()) (move This block around)
	//// bind the render target
	//m_renderTarget.bind();
	//
	//// clear the back buffer
	//clearScreen();
	//// END BLOCK
	//
	//DrawTexPhongArthur();
	//
	//// RENDER TARGET BLOCK
	//// bind the shader on the image on the quad to draw the render target to
	//DrawRenderTargetTex();
	//// END BLOCK

	DrawTexPhongSpear();
	
	DrawTexPhongRock();

	DrawTexPhongArthur();

	DrawTexBlendQuad();

	DrawTexTree();

	DrawTexTree2();

	DrawParticles();
}

//--------------------------------------------------------------------------------------
// cleans up the application, destroys the window, terminates glfw
//
// Return:
//		Returns 0 for success.
//--------------------------------------------------------------------------------------
int Application::terminate()
{
	// passing in the window to be destroyed
	glfwDestroyWindow(window);

	// finished with glfw / shutdown glfw / severe link to gpu
	glfwTerminate();

	return 0;
}

//--------------------------------------------------------------------------------------
// Links the appropriate shader and mesh then sets it location and scale.
//
// Return:
//		Returns false if the shader is invalid or the model cannot be loaded.
//--------------------------------------------------------------------------------------
bool Application::InitTexPhongSpear()
{
	// load vertex shader from file
	m_phongShader.loadShader(aie::eShaderStage::VERTEX, "../shaders/phongmap.vert");
	// load fragment shader from file
	m_phongShader.loadShader(aie::eShaderStage::FRAGMENT, "../shaders/phongmap.frag");

	if (m_phongShader.link() == false)
	{
		printf("Shader Error: %s\n", m_phongShader.getLastError());
		return false;
	}

	if (m_spearMesh.load("../soulspear/soulspear.obj",
		true, true) == false) {
		printf("Soulspear Mesh Error!\n");
		return false;
	}

	// set location and scale
	m_spearTransform = {
		1,0,0,0,
		0,1,0,0,
		0,0,1,0,
		0,2.4,0,1
	};

	return true;
}

//--------------------------------------------------------------------------------------
// Draws the model applied in the function above.
//--------------------------------------------------------------------------------------
void Application::DrawTexPhongSpear()
{
	// bind transform
	// bind phong shader program
	m_phongShader.bind();

	// bind light
	m_phongShader.bindUniform("Ia", m_ambientLight3);
	m_phongShader.bindUniform("Id", m_light3.diffuse);
	m_phongShader.bindUniform("Is", m_light3.specular);
	m_phongShader.bindUniform("LightDirection", m_light3.direction);

	// bind transform
	auto pvmspear = flycam->getProjectionView() * m_spearTransform;
	m_phongShader.bindUniform("ProjectionViewModel", pvmspear);

	m_phongShader.bindUniform("cameraPosition", flycam->getPosition());

	//std::cout << flycam->getPosition().x << " " << flycam->getPosition().y << " " << flycam->getPosition().z << std::endl;

	// bind transforms for lighting
	m_phongShader.bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_spearTransform)));

	// draw spear
	m_spearMesh.draw();
}

//--------------------------------------------------------------------------------------
// Links the appropriate shader and mesh then sets it location and scale.
//
// Return:
//		Returns false if the shader is invalid or the model cannot be loaded.
//--------------------------------------------------------------------------------------
bool Application::InitTexPhongRock()
{
	// load vertex shader from file
	m_phongShader.loadShader(aie::eShaderStage::VERTEX, "../shaders/phongmap.vert");
	// load fragment shader from file
	m_phongShader.loadShader(aie::eShaderStage::FRAGMENT, "../shaders/phongmap.frag");

	if (m_phongShader.link() == false)
	{
		printf("Shader Error: %s\n", m_phongShader.getLastError());
		return false;
	}

	if (m_rockMesh.load("../models/Rock/Rock.obj",
		true, true) == false) {
		printf("Rock Mesh Error!\n");
		return false;
	}

	// set location and scale
	m_rockTransform = {
		2.5,0,0,0,
		0,2.5,0,0,
		0,0,2.5,0,
		0,0,0,1
	};

	// rotate the rock
	glm::quat rot = glm::angleAxis(glm::pi<float>() * 1.3f, glm::vec3(0, 1, 0));
	m_rockTransform = glm::mat4_cast(rot) * m_rockTransform;

	return true;
}

//--------------------------------------------------------------------------------------
// Draws the model applied in the function above.
//--------------------------------------------------------------------------------------
void Application::DrawTexPhongRock()
{
	// bind transform
	// bind phong shader program
	m_phongShader.bind();

	// bind light
	m_phongShader.bindUniform("Ia", m_ambientLight4);
	m_phongShader.bindUniform("Id", m_light4.diffuse);
	m_phongShader.bindUniform("Is", m_light4.specular);
	m_phongShader.bindUniform("LightDirection", m_light4.direction);

	// bind transform
	auto pvmrock = flycam->getProjectionView() * m_rockTransform;
	m_phongShader.bindUniform("ProjectionViewModel", pvmrock);

	m_phongShader.bindUniform("cameraPosition", flycam->getPosition());

	// bind transforms for lighting
	m_phongShader.bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_rockTransform)));

	// draw rock
	m_rockMesh.draw();
}

//--------------------------------------------------------------------------------------
// Links the appropriate shader and mesh then sets it location and scale.
//
// Return:
//		Returns false if the shader is invalid or the model cannot be loaded.
//--------------------------------------------------------------------------------------
bool Application::InitTexPhongArthur()
{
	// load vertex shader from file
	m_phongShader.loadShader(aie::eShaderStage::VERTEX, "../shaders/phongmap.vert");
	// load fragment shader from file
	m_phongShader.loadShader(aie::eShaderStage::FRAGMENT, "../shaders/phongmap.frag");

	if (m_phongShader.link() == false)
	{
		printf("Shader Error: %s\n", m_phongShader.getLastError());
		return false;
	}

	if (m_arthurMesh.load("../models/arthur/arthur.obj",
		true, true) == false) {
		printf("arthur Mesh Error!\n");
		return false;
	}

	// set location and scale
	m_arthurTransform = {
		8,0,0,0,
		0,8,0,0,
		0,0,8,0,
		0,0,-6,1
	};

	// rotate arthur
	glm::quat rot = glm::angleAxis(glm::pi<float>() * 1.0f, glm::vec3(0, 1, 0));
	m_arthurTransform = glm::mat4_cast(rot) * m_arthurTransform;

	return true;
}

//--------------------------------------------------------------------------------------
// Draws the model applied in the function above.
//--------------------------------------------------------------------------------------
void Application::DrawTexPhongArthur()
{
	// bind transform
	// bind phong shader program
	m_phongShader.bind();

	// bind light
	m_phongShader.bindUniform("Ia", m_ambientLight5);
	m_phongShader.bindUniform("Id", m_light5.diffuse);
	m_phongShader.bindUniform("Is", m_light5.specular);
	m_phongShader.bindUniform("LightDirection", m_light5.direction);

	// bind transform
	auto pvmarthur = flycam->getProjectionView() * m_arthurTransform;
	m_phongShader.bindUniform("ProjectionViewModel", pvmarthur);

	m_phongShader.bindUniform("cameraPosition", flycam->getPosition());

	// bind transforms for lighting
	m_phongShader.bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_arthurTransform)));

	// draw arthur
	m_arthurMesh.draw();
}

//--------------------------------------------------------------------------------------
// Links the appropriate shader and mesh then sets it location and scale.
//
// Return:
//		Returns false if the shader is invalid or the model cannot be loaded.
//--------------------------------------------------------------------------------------
bool Application::InitTexBlendQuad()
{
	// load vertex shader from file
	m_blendShader.loadShader(aie::eShaderStage::VERTEX, "../shaders/texblend.vert");
	// load fragment shader from file
	m_blendShader.loadShader(aie::eShaderStage::FRAGMENT, "../shaders/texblend.frag");

	if (m_blendShader.link() == false)
	{
		printf("Shader Error: %s\n", m_blendShader.getLastError());
		return false;
	}

	// load grass texture
	if (grass.load("../models/BlendQuad/grass.png") == false) {
		printf("Failed to load texture!\n");
		return false;
	}

	// load cobblestone texture
	if (path.load("../models/BlendQuad/path.png") == false) {
		printf("Failed to load texture!\n");
		return false;
	}

	// load river texture
	if (river.load("../models/BlendQuad/river.png") == false) {
		printf("Failed to load texture!\n");
		return false;
	}

	// mirros the texture along its centre
	glBindTexture(GL_TEXTURE_2D, river.getHandle());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

	// load blendmap texture
	if (blendMap.load("../models/BlendQuad/blendmapcolor.png") == false) {
		printf("Failed to load texture!\n");
		return false;
	}
	
	m_blendQuadMesh.initialiseQuad();

	// set location and scale
	m_blendQuadTransform = {
		15,0,0,0,
		0,15,0,0,
		0,0,15,0,
		0,0,0,1
	};

	return true;
}

//--------------------------------------------------------------------------------------
// Draws the model applied in the function above.
//--------------------------------------------------------------------------------------
void Application::DrawTexBlendQuad()
{
	// unbind phong shader so we can use another shader program
	//m_phongShader.unbind();

	// bind blend shader program
	m_blendShader.bind();

	m_blendShader.bindUniform("time", (float)glfwGetTime());

	// bind transform
	auto pvmquad = flycam->getProjectionView() * m_blendQuadTransform;
	m_blendShader.bindUniform("ProjectionViewModel", pvmquad);


	// bind texture location
	m_blendShader.bindUniform("grassTexture", 0);

	// bind texture to specified location
	grass.bind(0);

	// bind texture location
	m_blendShader.bindUniform("cobbleTexture", 1);

	// bind texture to specified location
	path.bind(1);

	// bind texture location
	m_blendShader.bindUniform("riverTexture", 2);

	// bind texture to specified location
	river.bind(2);

	// bind texture location
	m_blendShader.bindUniform("tintTexture", 3);

	// bind texture to specified location
	blendMap.bind(3);

	// draw blendQuad
	m_blendQuadMesh.draw();
}

//--------------------------------------------------------------------------------------
// Links the appropriate shader and mesh then sets it location and scale.
//
// Return:
//		Returns false if the shader is invalid or the model cannot be loaded.
//--------------------------------------------------------------------------------------
bool Application::InitTexTree()
{
	// load vertex shader from file
	m_phongShader.loadShader(aie::eShaderStage::VERTEX, "../shaders/phongmap.vert");
	// load fragment shader from file
	m_phongShader.loadShader(aie::eShaderStage::FRAGMENT, "../shaders/phongmap.frag");

	if (m_phongShader.link() == false)
	{
		printf("Shader Error: %s\n", m_phongShader.getLastError());
		return false;
	}

	if (m_treeMesh.load("../models/tree/tree1.obj",
		true, true) == false) {
		printf("tree Mesh Error!\n");
		return false;
	}

	// set location and scale
	m_treeTransform = {
		8,0,0,0,
		0,8,0,0,
		0,0,8,0,
		-6,0,-6,1
	};

	return true;
}

//--------------------------------------------------------------------------------------
// Draws the model applied in the function above.
//--------------------------------------------------------------------------------------
void Application::DrawTexTree()
{
	// bind transform
	// bind phong shader program
	m_phongShader.bind();

	// bind light
	m_phongShader.bindUniform("Ia", m_ambientLight1);
	m_phongShader.bindUniform("Id", m_light1.diffuse);
	m_phongShader.bindUniform("Is", m_light1.specular);
	m_phongShader.bindUniform("LightDirection", m_light1.direction);

	// bind transform
	auto pvmtree = flycam->getProjectionView() * m_treeTransform;
	m_phongShader.bindUniform("ProjectionViewModel", pvmtree);

	m_phongShader.bindUniform("cameraPosition", flycam->getPosition());

	// bind transforms for lighting
	m_phongShader.bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_treeTransform)));

	// draw tree
	m_treeMesh.draw();
}

//--------------------------------------------------------------------------------------
// Links the appropriate shader and mesh then sets it location and scale.
//
// Return:
//		Returns false if the shader is invalid or the model cannot be loaded.
//--------------------------------------------------------------------------------------
bool Application::InitTexTree2()
{
	// load vertex shader from file
	m_phongShader.loadShader(aie::eShaderStage::VERTEX, "../shaders/phongmap.vert");
	// load fragment shader from file
	m_phongShader.loadShader(aie::eShaderStage::FRAGMENT, "../shaders/phongmap.frag");

	if (m_phongShader.link() == false)
	{
		printf("Shader Error: %s\n", m_phongShader.getLastError());
		return false;
	}

	if (m_tree2Mesh.load("../models/tree/tree1.obj",
		true, true) == false) {
		printf("tree2 Mesh Error!\n");
		return false;
	}

	// set location and scale
	m_tree2Transform = {
		8,0,0,0,
		0,8,0,0,
		0,0,8,0,
		6,0,-6,1
	};

	return true;
}

//--------------------------------------------------------------------------------------
// Draws the model applied in the function above.
//--------------------------------------------------------------------------------------
void Application::DrawTexTree2()
{
	// bind transform
	// bind phong shader program
	m_phongShader.bind();

	// bind light
	m_phongShader.bindUniform("Ia", m_ambientLight3);
	m_phongShader.bindUniform("Id", m_light3.diffuse);
	m_phongShader.bindUniform("Is", m_light3.specular);
	m_phongShader.bindUniform("LightDirection", m_light3.direction);

	// bind transform
	auto pvmtree2 = flycam->getProjectionView() * m_tree2Transform;
	m_phongShader.bindUniform("ProjectionViewModel", pvmtree2);

	m_phongShader.bindUniform("cameraPosition", flycam->getPosition());

	// bind transforms for lighting
	m_phongShader.bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_tree2Transform)));

	// draw tree2
	m_tree2Mesh.draw();
}

//--------------------------------------------------------------------------------------
// Links the appropriate shader and creates a particle system emmiter then sets it spawn
// location and scale.
//
// Return:
//		Returns false if the shader is invalid.
//--------------------------------------------------------------------------------------
bool Application::InitParticles()
{
	// load vertex shader from file
	m_particleShader.loadShader(aie::eShaderStage::VERTEX, "../shaders/particle.vert");
	// load fragment shader from file
	m_particleShader.loadShader(aie::eShaderStage::FRAGMENT, "../shaders/particle.frag");

	if (m_particleShader.link() == false)
	{
		printf("Shader Error: %s\n", m_particleShader.getLastError());
		return false;
	}

	// creating particle emitter
	// and setting up the values
	m_emitter = new ParticleEmitter();
	m_emitter->initalise(250, 25, 1.5f, 2.5f, 1, 5, 0.75f, 0.1f, glm::vec4(0.5f, 0.5f, 1, 1), glm::vec4(1, 0, 1, 0.5f));

	// set location and scale
	m_particleTransform = {
		1,0,0,0,
		0,1,0,0,
		0,0,1,0,
		0,15,0,1
	};

	return true;
}

//--------------------------------------------------------------------------------------
// Draws the particles.
//--------------------------------------------------------------------------------------
void Application::DrawParticles()
{
	// bind particle shader
	m_particleShader.bind();
	// bind particle transform
	auto pvm = flycam->getProjectionView() * m_particleTransform;
	m_particleShader.bindUniform("ProjectionViewModel", pvm);
	// draw the pretty particles
	m_emitter->draw();
}

//--------------------------------------------------------------------------------------
// Initialises the render target and and prepares it to be drawn.
//
// Return:
//		Returns false if the shader cannot be loaded.
//--------------------------------------------------------------------------------------
bool Application::InitRenderTargetTex()
{
	// load vertex shader from file
	m_texturedShader.loadShader(aie::eShaderStage::VERTEX,
		"../shaders/textured.vert");

	// load fragment shader from file
	m_texturedShader.loadShader(aie::eShaderStage::FRAGMENT,
		"../shaders/textured.frag");

	if (m_texturedShader.link() == false)
	{
		printf("Shader Error: %s\n", m_texturedShader.getLastError());
		return false;
	}

	if (m_gridTexture.load("../texture/mytexture.tga") == false) {
		printf("Failed to load texture!\n");
		return false;
	}

	m_renderTarQuadMesh.initialiseQuad();

	// set location and scale
	m_renderTarQuadTransform = {
		19,0,0,0,
		0,19,0,0,
		0,0,19,0,
		0,0,0,1 };

	return true;
}

//--------------------------------------------------------------------------------------
// Draws the buffer the quad we are using.
//--------------------------------------------------------------------------------------
void Application::DrawRenderTargetTex()
{
	// bind shader
	m_texturedShader.bind();

	// bind transform
	auto pvm = flycam->getProjectionView() * m_renderTarQuadTransform;
	m_texturedShader.bindUniform("ProjectionViewModel", pvm);

	// we are finished drawing things so we unbind the render target
	//m_renderTarget.unbind();

	// clear the back buffer
	clearScreen();

	// bind texture location
	m_texturedShader.bindUniform("diffuseTexture", 0);

	// bind texture to specified location
	m_renderTarget.getTarget(0).bind(0);

	// draw quad with texture
	m_renderTarQuadMesh.draw();
}