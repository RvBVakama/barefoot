#include "RenderTarget.h"
#include "gl_core_4_5.h"
#include <vector>

namespace aie
{
	//--------------------------------------------------------------------------------------
	// RenderTarget object
	// Creates another buffer in which we can store images/textures. 
	// This allows us to render a frame while holding one in this buffer.
	// This allows us to apply effects to it or use it as a mirror's reflection.
	//--------------------------------------------------------------------------------------
	RenderTarget::RenderTarget() : m_width(0), m_height(0), m_targetCount(0), m_targets(nullptr)
	{

	}

	//--------------------------------------------------------------------------------------
	// Alternative Constructor
	// 
	// Parameters:
	//		targetCount: How many images/texture bufers to create.
	//		width: width of those images/textures buffers.
	//		height: height of those images/textures buffers.
	//--------------------------------------------------------------------------------------
	RenderTarget::RenderTarget(unsigned int targetCount, unsigned int width, unsigned int height)
		: m_width(0), m_height(0), m_targetCount(0), m_targets(nullptr)
	{
		initialise(targetCount, width, height);
	}

	//--------------------------------------------------------------------------------------
	// Default Destructor
	//--------------------------------------------------------------------------------------
	RenderTarget::~RenderTarget()
	{
		delete[] m_targets;
		glDeleteRenderbuffers(1, &m_rbo);
		glDeleteFramebuffers(1, &m_fbo);
	}

	//--------------------------------------------------------------------------------------
	// Runs initialize() and checks if the window should close if esc was pressed.
	//
	// Parameters:
	//		targetCount: How many images/texture bufers to create.
	//		width: width of those images/textures buffers.
	//		height: height of those images/textures buffers.
	//
	// Returns true or false if it ran successfully or not.
	//--------------------------------------------------------------------------------------
	bool RenderTarget::initialise(unsigned int targetCount, unsigned int width, unsigned int height)
	{

		// setup and bind a framebuffer object
		glGenFramebuffers(1, &m_fbo);
		glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

		// create and attach textures
		if (targetCount > 0) {

			m_targets = new Texture[targetCount];

			std::vector<GLenum> drawBuffers = {};

			for (unsigned int i = 0; i < targetCount; ++i)
			{

				m_targets[i].create(width, height, Texture::RGBA);

				drawBuffers.push_back(GL_COLOR_ATTACHMENT0 + i);

				glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i,
					m_targets[i].getHandle(), 0);
			}

			glDrawBuffers((GLsizei)drawBuffers.size(), drawBuffers.data());
		}

		// setup and bind a 24bit depth buffer as a render buffer
		glGenRenderbuffers(1, &m_rbo);
		glBindRenderbuffer(GL_RENDERBUFFER, m_rbo);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24,
			width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
			GL_RENDERBUFFER, m_rbo);

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{

			// cleanup
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			delete[] m_targets;
			m_targets = nullptr;
			glDeleteRenderbuffers(1, &m_rbo);
			glDeleteFramebuffers(1, &m_fbo);
			m_rbo = 0;
			m_fbo = 0;

			return false;
		}

		// success
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		m_targetCount = targetCount;
		m_width = width;
		m_height = height;

		return true;
	}

	//--------------------------------------------------------------------------------------
	// Binds the framebuffer to the frame buffer object that we created.
	//--------------------------------------------------------------------------------------
	void RenderTarget::bind()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	}

	//--------------------------------------------------------------------------------------
	// Unbinds the framebuffer by setting it to zero.
	//--------------------------------------------------------------------------------------
	void RenderTarget::unbind()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}