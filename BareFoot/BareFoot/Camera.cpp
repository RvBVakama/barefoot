#include "Camera.h"
#include <ext.hpp>

//--------------------------------------------------------------------------------------
// Default Constructor
// Sets up default values.
//--------------------------------------------------------------------------------------
Camera::Camera()
{
	// assigning default values
	worldTransform = mat4(0);
	viewTransform = mat4(0);
	projectionTransform = mat4(0);
	projectionViewTransform = mat4(0);
	
	// camera view transform
	// camera view transform is at 10,10,10 looking at the world's center
	setLookAt(vec3(10, 10, 10), vec3(0), vec3(0, 1, 0));

	fov = glm::pi<float>() * fovAmount.original;	

	// camera projection transform
	setPerspective(fov, 16.0f / 9.0f, 0.1f, 1000.0f);
}

//--------------------------------------------------------------------------------------
// Default Destructor
//--------------------------------------------------------------------------------------
Camera::~Camera()
{
}

//--------------------------------------------------------------------------------------
// updates the projection view transform
//--------------------------------------------------------------------------------------
void Camera::updateProjectionViewTransform()
{
	projectionViewTransform = projectionTransform * viewTransform;
}

//--------------------------------------------------------------------------------------
// Sets the perspective of the camera.
//
//	Parameters:
//		fieldOfView: FOV.
//		aspectRatio: aspect ratio.
//		near: near clip plane.
//		far: far clip plane.
//--------------------------------------------------------------------------------------
void Camera::setPerspective(const float &fieldOfView, const float &aspectRatio, const float &near, const float &far)
{
	projectionTransform = glm::perspective(fieldOfView, aspectRatio, near, far);
	updateProjectionViewTransform();
}

//--------------------------------------------------------------------------------------
// Sets the perspective of the camera.
//
//	Parameters:
//		from: position where it looks from.
//		to: where it will look.
//		up: define its y axis.
//--------------------------------------------------------------------------------------
void Camera::setLookAt(const vec3 &from, const vec3 &to, const vec3 &up)
{
	viewTransform = glm::lookAt(from, to, up);
}

//--------------------------------------------------------------------------------------
// Sets the position of the camera.
//
//	Parameter:
//		position: position where it goes to.
//--------------------------------------------------------------------------------------
void Camera::setPos(const vec3 &position)
{
	worldTransform[3] = glm::vec4(position, worldTransform[3][3]);
	updateProjectionViewTransform();
}

//--------------------------------------------------------------------------------------
// Gets the world transform.
//
// Return:
//		Returns the world transform.
//--------------------------------------------------------------------------------------
mat4 Camera::getWorldTransform() const
{
	return worldTransform;
}

//--------------------------------------------------------------------------------------
// Gets the view transform.
//
// Return:
//		Returns the view transform.
//--------------------------------------------------------------------------------------
mat4 Camera::getView() const
{
	return viewTransform;
}

//--------------------------------------------------------------------------------------
// Gets the projection transform.
//
// Return:
//		Returns the projection transform.
//--------------------------------------------------------------------------------------
mat4 Camera::getProjection() const
{
	return projectionTransform;
}

//--------------------------------------------------------------------------------------
// Gets the projection View Transform.
//
// Return:
//		Returns the projection View Transform.
//--------------------------------------------------------------------------------------
mat4 Camera::getProjectionView() const
{
	return projectionViewTransform;
}

//--------------------------------------------------------------------------------------
// Gets the camera's position.
//
// Return:
//		Returns the camera's position
//--------------------------------------------------------------------------------------
vec3 Camera::getPosition()
{
	vec3 v3;

	v3.x = worldTransform[3].x;
	v3.y = worldTransform[3].y;
	v3.z = worldTransform[3].z;
	
	return v3;
}

