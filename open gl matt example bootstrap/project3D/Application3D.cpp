#include "Application3D.h"
#include "Gizmos.h"
#include "Input.h"
#include "Shader.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <gl_core_4_4.h>

#include <iostream>





using glm::vec3;
using glm::vec4;
using glm::mat4;
using aie::Gizmos;


Application3D::Application3D() {

}

Application3D::~Application3D() {

}

bool Application3D::startup() {
	
	setBackgroundColour(0.25f, 0.25f, 0.25f);

	// initialise gizmo primitive counts
	Gizmos::create(10000, 10000, 10000, 10000);

	// create simple camera transforms
	m_viewMatrix = glm::lookAt(vec3(10), vec3(0), vec3(0, 1, 0));
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f,
										  getWindowWidth() / (float)getWindowHeight(),
										  0.1f, 1000.f);


	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);


	struct vertex
	{
		glm::vec4 position;
		glm::vec4 color;
	};

	std::vector<vertex> verts = {
		{ { -1, -1, 0, 1 }, {1, 0, 0, 1} },
		{ { 1, -1, 0, 1 }, { 1, 0, 1, 1 } },
		{ { 1, 1, 0, 1 }, { 1, 1, 0, 1 } },
		{ { -1, 1, 0, 1 }, { 0, 1, 1, 1 } },
	};

	std::vector<unsigned int> indicies = {
		0, 1, 3,
		1, 2, 3
	};

	
	glGenVertexArrays(1, &quadData.m_vao);
	glBindVertexArray(quadData.m_vao);

	glGenBuffers(1, &quadData.m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, quadData.m_vbo);
	glBufferData(GL_ARRAY_BUFFER, verts.size() * sizeof(vertex), verts.data(), GL_STATIC_DRAW);

	glGenBuffers(1, &quadData.m_ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadData.m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicies.size() * sizeof(unsigned int), indicies.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)offsetof(vertex, position));


	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)offsetof(vertex, color));

	quadData.m_indexCount = indicies.size();

	glBindVertexArray(0);

	m_basicShader.loadShader(aie::eShaderStage::VERTEX, "basic.vert");
	m_basicShader.loadShader(aie::eShaderStage::FRAGMENT, "basic.frag");
	
	if (m_basicShader.link() == false)
	{
		std::cout << "Shader error: " << m_basicShader.getLastError() << std::endl;
		return false;
	}

	return true;
}

void Application3D::shutdown() {

	glDeleteBuffers(1, &quadData.m_vbo);
	glDeleteBuffers(1, &quadData.m_ibo);
	glDeleteVertexArrays(1, &quadData.m_vao);

	Gizmos::destroy();
}

void Application3D::update(float deltaTime) {

	// query time since application started
	float time = getTime();

	// rotate camera
	//m_viewMatrix = glm::lookAt(vec3(glm::sin(time) * 10, 10, glm::cos(time) * 10),
	//						   vec3(0), vec3(0, 1, 0));

	// wipe the gizmos clean for this frame
	Gizmos::clear();

	// draw a simple grid with gizmos
	vec4 white(1);
	vec4 black(0, 0, 0, 1);
	for (int i = 0; i < 21; ++i) {
		Gizmos::addLine(vec3(-10 + i, 0, 10),
						vec3(-10 + i, 0, -10),
						i == 10 ? white : black);
		Gizmos::addLine(vec3(10, 0, -10 + i),
						vec3(-10, 0, -10 + i),
						i == 10 ? white : black);
	}

	// add a transform so that we can see the axis
	Gizmos::addTransform(mat4(1));

	// quit if we press escape
	aie::Input* input = aie::Input::getInstance();

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Application3D::draw() {

	// wipe the screen to the background colour
	clearScreen();



	// update perspective in case window resized
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f,
										  getWindowWidth() / (float)getWindowHeight(),
										  0.1f, 1000.f);


	m_basicShader.bind();

	glUseProgram(m_basicShader.getHandle());

	// pass in the pvm to the shader so it can use it how it likes!
	m_basicShader.bindUniform("PVM", (m_projectionMatrix * m_viewMatrix));
	
	
	glm::mat4 pvm = m_projectionMatrix * m_viewMatrix;
	int pvm_handle = glGetUniformLocation(m_basicShader.getHandle(), "PVM");
	//assert(pvm_handle >= 0 && "Couldn't find handle");
	glUniformMatrix4fv(pvm_handle, 1, GL_FALSE, glm::value_ptr(pvm));


	glBindVertexArray(quadData.m_vao);
	glDrawElements(GL_TRIANGLES, quadData.m_indexCount, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	// draw 3D gizmos
	Gizmos::draw(m_projectionMatrix * m_viewMatrix);

	// draw 2D gizmos using an orthogonal projection matrix (or screen dimensions)
	Gizmos::draw2D((float)getWindowWidth(), (float)getWindowHeight());
}