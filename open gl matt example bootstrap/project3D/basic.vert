#version 410

layout(location=0) in vec4 position;
layout(location=1) in vec4 color;
layout(location=2) in vec2 texCoords;

out vec4 vColor;
out vec2 vTexCoord;

uniform mat4 PVM;

// for skybox rotation
uniform mat4 cameraRotation;

void main()
{
    vColor = color;

    // stretch texture to screen because it is already -1 to 1
    // otherwise scale down to -1 to 1 
    //gl_Position = position;
    
    // normal rendering, see the quad in 3d space
    gl_Position = PVM * position;
    
    // for use with skybox, camera rotation takes into account the cameras rotation
    // and moves the skybox to accomadate that
    //gl_Position = cameraRotation * position;
}